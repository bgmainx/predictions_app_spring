package com.isoft.predictions_app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PredictionsAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(PredictionsAppApplication.class, args);
	}

}
